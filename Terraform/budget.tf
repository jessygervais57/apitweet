// Get subscription details
data "azurerm_subscription" "subscription" {}

variable "budgets" {
  description = "Liste des budgets"
  type = list(object({
    name       = string
    amount     = number
    time_grain = string
    start_date = string
    end_date   = string
    notifications = list(object({
      operator       = string  // Valid values are: "EqualTo", "GreaterThan", "GreaterThanOrEqualTo"
      threshold      = string  // Valid values are: "Actual", "Forecasted"
      enabled        = bool
      contact_emails = list(string)
      contact_roles  = list(string)
    }))
  }))

  default = [
    {
      name = "budget_max"
      amount = 5
      time_grain = "Monthly"
      start_date = "2023-08-01T00:00:00Z"
      end_date = "2024-08-31T00:00:00Z"
      notifications = [
        {
          enabled = true
          operator       = "GreaterThan"
          threshold      = 6
          contact_emails = ["jessygervais57@gmail.com"]
          contact_roles = ["Owner"]
        }
      ]
    },
  ]
}


// Create consumption budgets
resource "azurerm_consumption_budget_subscription" "budget" {
  for_each = { for b in var.budgets : b.name => b }

  name            = each.value.name
  amount          = each.value.amount
  time_grain      = each.value.time_grain
  subscription_id = data.azurerm_subscription.subscription.id

  // Set the time period for the budget
  time_period {
    start_date = each.value.start_date
    end_date   = each.value.end_date
  }

  // Create notifications for each budget
  dynamic "notification" {
    for_each = each.value.notifications
    content {
      enabled        = notification.value.enabled
      contact_emails = notification.value.contact_emails
      contact_roles  = notification.value.contact_roles
      operator       = notification.value.operator
      threshold      = notification.value.threshold
    }
  }
}

// Output the IDs of the created budgets
output "budget_ids" {
  description = "The IDs of the created budgets"
  value       = { for b in azurerm_consumption_budget_subscription.budget : b.name => b.id }
}

// Output the names of the created budgets
output "budget_names" {
  description = "The names of the created budgets"
  value       = [for b in azurerm_consumption_budget_subscription.budget : b.name]
}
