resource "azurerm_resource_group" "apitweet" {
  name     = "apitweet"
  location = "West Europe"
}

resource "azurerm_app_service_plan" "apitweet" {
  name                = "apitweet-app-service-plan"
  location            = azurerm_resource_group.apitweet.location
  resource_group_name = azurerm_resource_group.apitweet.name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "apitweet" {
  name                = "apitweet-app-service"
  location            = azurerm_resource_group.apitweet.location
  resource_group_name = azurerm_resource_group.apitweet.name
  app_service_plan_id = azurerm_app_service_plan.apitweet.id

  site_config {
    always_on       = true
    linux_fx_version = "DOCKER|gervaisjessy/apitweet:0.0.1"
  }

  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
  }
}

output "app_service_url" {
  value = azurerm_app_service.apitweet.default_site_hostname
}



