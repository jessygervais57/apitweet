import re

from django.shortcuts import render
from django.http import JsonResponse
import pickle

from django.views.decorators.csrf import csrf_exempt




def clean_tweet(tweet):

    stopwords = ["a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with"]
    if type(tweet) == float:
        return ""
    temp = tweet.lower()
    temp = re.sub("'", "", temp) # to avoid removing contractions in english
    temp = re.sub("@[A-Za-z0-9_]+","", temp)
    temp = re.sub("#[A-Za-z0-9_]+","", temp)
    temp = re.sub(r'http\S+', '', temp)
    temp = re.sub('[()!?]', ' ', temp)
    temp = re.sub('\[.*?\]',' ', temp)
    temp = re.sub("[^a-z0-9]"," ", temp)
    temp = temp.split()
    temp = [w for w in temp if not w in stopwords]
    temp = " ".join(word for word in temp)
    return temp

@csrf_exempt
def detect_tweet(request):
    if request.method == 'POST':
        tweet = request.POST.get('tweet')

        # Charger le modèle à partir du fichier
        with open('TweetApp/static/TweetApp/rf.pkl', 'rb') as file:
            model = pickle.load(file)
        with open('TweetApp/static/TweetApp/vectorizer.pkl', 'rb') as file:
            vectorizer = pickle.load(file)


        tweet = clean_tweet(tweet)
        vectorized_tweet = vectorizer.transform([tweet])

        classification = model.predict(vectorized_tweet)[0]

        if classification == 0:
            classification = "Tweet haineux"
        else:
            classification = "Tweet non haineux"

        return render(request, 'TweetApp/index.html', {'classification': classification})

    return render(request, 'TweetApp/index.html')
